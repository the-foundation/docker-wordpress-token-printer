# Docker .env printer for wordpress

## INSTALLING

* clone the repo
* link wptoken to `/usr/bin/wptoken`
* link wpdbtoken to `/usr/bin/wpdbtoken`
(e.g. `ln -s /etc/custom/docker-wordpress-token-printer/wp* /usr/bin/`)
* make them executable if something went wrong or you did not clone from git  (e.g. `chmod +x /etc/custom/docker-wordpress-token-printer/wp*` )

* add an entry to `/etc/sudoers`

```
%groupname ALL=(ALL) NOPASSWD: /usr/bin/wptoken *
%groupname ALL=(ALL) NOPASSWD: /usr/bin/wpdbtoken *
```
* add users to the group
---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-wordpress-token-printer/README.md/logo.jpg" width="480" height="270"/></div></a>
